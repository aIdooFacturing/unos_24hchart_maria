<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:message var="layout" code="layout"></spring:message>
<spring:message var="kr" code="kr"></spring:message>
<spring:message var="na" code="na"></spring:message>
<spring:message var="up" code="up"></spring:message>
<spring:message var="am" code="am"></spring:message>
<spring:message var="pm" code="pm"></spring:message>
<spring:message var="worker_manage" code="worker_manage"></spring:message>
<spring:message var="machine_list" code="machine_list"></spring:message>
<spring:message var="alarm_manage" code="alarm_manage"></spring:message>
<spring:message var="check_result_manage" code="check_result_manage"></spring:message>
<spring:message var="op_ratio" code="op_ratio"></spring:message>
<spring:message var="in_check" code="in_check"></spring:message>
<spring:message var="check" code="check"></spring:message>
<spring:message var="js_check" code="js_check"></spring:message>
<spring:message var="jr_check" code="jr_check"></spring:message>
<spring:message var="add_prdct_target" code="add_prdct_target"></spring:message>
<spring:message var="add_cmpl_prdct" code="add_cmpl_prdct"></spring:message>
<spring:message var="month" code="month"></spring:message>
<spring:message var="non_op" code="non_op"></spring:message>
<spring:message var="end_time" code="end_time"></spring:message>
<spring:message var="fixed_time" code="fixed_time"></spring:message>
<spring:message var="content" code="content"></spring:message>
<spring:message var="worker" code="worker"></spring:message>
<spring:message var="alarm" code="alarm"></spring:message>
<spring:message var="start_time" code="start_time"></spring:message>
<spring:message var="unnormal" code="unnormal"></spring:message>
<spring:message var="date_" code="date_"></spring:message>
<spring:message var="op_program" code="op_program"></spring:message>
<spring:message var="spd_err_g" code="spd_err_g"></spring:message>
<spring:message var="cycle_time" code="cycle_time"></spring:message>
<spring:message var="cycle_time_avrg" code="cycle_time_avrg"></spring:message>
<spring:message var="cycle_time_sd" code="cycle_time_sd"></spring:message>
<spring:message var="spd_load_sum" code="spd_load_sum"></spring:message>
<spring:message var="cuttingratio" code="cuttingratio"></spring:message>
<spring:message var="no_data" code="no_data"></spring:message>
<spring:message var="normal" code="normal"></spring:message>
<spring:message var="spd_load_sum_avrg" code="spd_load_sum_avrg"></spring:message>
<spring:message var="spd_load_sum_avrg_sd" code="spd_load_sum_avrg_sd"></spring:message>
<spring:message var="sample_cnt" code="sample_cnt"></spring:message>
<spring:message var="cycle_start" code="cycle_start"></spring:message>
<spring:message var="add_cmpl_prdct_history" code="add_cmpl_prdct_history"></spring:message>
<spring:message var="add_non_operation" code="add_non_operation"></spring:message>
<spring:message var="add_non_operation_hist" code="add_non_operation_hist"></spring:message>
<spring:message var="total" code="total"></spring:message>
<spring:message var="end_of_chart" code="end_of_chart"></spring:message>
<spring:message var="first_of_chart" code="first_of_chart"></spring:message>
<spring:message var="plan" code="plan"></spring:message>
<spring:message var="machine_performance" code="machine_performance"></spring:message>
<spring:message var="worker_performance" code="worker_performance"></spring:message>
<spring:message var="lot_tracer" code="lot_tracer"></spring:message>
<spring:message var="add_faulty_hist" code="add_faulty_hist"></spring:message>
<spring:message var="faulty" code="faulty"></spring:message>
<spring:message var="reoperation_cycle" code="reoperation_cycle"></spring:message>
<spring:message var="opstatus" code="opstatus"></spring:message>
<spring:message var="dailydevicestatus" code="dailydevicestatus"></spring:message>
<spring:message var="devicestatus" code="devicestatus"></spring:message>
<spring:message var="tool_manage" code="tool_manage"></spring:message>
<spring:message var="prdct_board" code="prdct_board"></spring:message>
<spring:message var="barchart" code="24barchart"></spring:message>
<spring:message var="performance_chart" code="performance_chart"></spring:message>
<spring:message var="operation_chart" code="operation_chart"></spring:message>
<spring:message var="prdct_chart" code="prdct_chart"></spring:message>
<spring:message var="operation_graph" code="operation_graph"></spring:message>
<spring:message var="operation_graph_daily" code="operation_graph_daily"></spring:message>
<spring:message var="check_program_error" code="check_program_error"></spring:message>
<spring:message var="income_manage" code="income_manage"></spring:message>
<spring:message var="income_history" code="income_history"></spring:message>
<spring:message code="device" var="device"></spring:message>
<spring:message code="program" var="program"></spring:message>
<spring:message code="stop" var="stop"></spring:message>
<spring:message code="noconnection" var="noconnection"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="wait" var="wait"></spring:message>
<spring:message code="avrg" var="avrg"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>
<spring:message var="release_manage" code="release_manage"></spring:message>
<spring:message var="release_history" code="release_history"></spring:message>
<spring:message var="ship_manage" code="ship_manage"></spring:message>
<spring:message var="income_no" code="income_no"></spring:message>
<spring:message var="prd_no" code="prd_no"></spring:message>
<spring:message var="com_name" code="com_name"></spring:message>
<spring:message var="lot_cnt" code="lot_cnt"></spring:message>
<spring:message var="check_cnt" code="check_cnt"></spring:message>
<spring:message var="faulty_cnt" code="faulty_cnt"></spring:message>
<spring:message var="faulty_history" code="faulty_history"></spring:message>
<spring:message var="add_faulty" code="add_faulty"></spring:message>
<spring:message var="income_date" code="income_date"></spring:message>
<spring:message var="del" code="del"></spring:message>
<spring:message var="selection" code="selection"></spring:message>
<spring:message var="prdct_cnt" code="prdct_cnt"></spring:message>
<spring:message var="prdct_per_cycle" code="prdct_per_cycle"></spring:message>
<spring:message var="working_time" code="working_time"></spring:message>
<spring:message var="uph_analysis" code="uph_analysis"></spring:message>
<spring:message var="prdct_machine_line" code="prdct_machine_line"></spring:message>
<spring:message var="performance_analysis" code="performance_analysis"></spring:message>
<spring:message var="working_capa" code="working_capa"></spring:message>
<spring:message var="prd_plan" code="prd_plan"></spring:message>
<spring:message var="ok_ratio" code="ok_ratio"></spring:message>
<spring:message var="spec" code="spec"></spring:message>
<spring:message var="achievement_ratio" code="achievement_ratio"></spring:message>
<spring:message var="worker_performance2" code="worker_performance2"></spring:message>
<spring:message var="diff" code="diff"></spring:message>
<spring:message var="prdct_performance" code="prdct_performance"></spring:message>
<spring:message var="day" code="day"></spring:message>
<spring:message var="copy_data" code="copy_data"></spring:message>
<spring:message var="deviceManager" code="deviceManager"></spring:message>
<spring:message var="layoutManager" code="layoutManager"></spring:message>
<spring:message var="minute" code="minute"></spring:message>
<spring:message var="second" code="second"></spring:message>
<spring:message var="night" code="night"></spring:message>
<spring:message var="income_cnt" code="income_cnt"></spring:message>
<spring:message var="release_prd" code="release_prd"></spring:message>
<spring:message var="prd_stock" code="prd_stock"></spring:message>
<spring:message var="complete_stock" code="complete_stock"></spring:message>
<spring:message var="ship_lot_no" code="ship_lot_no"></spring:message>
<spring:message var="lot_no" code="lot_no"></spring:message>
<spring:message var="release_count" code="release_count"></spring:message>
<spring:message var="op_order" code="op_order"></spring:message>
<spring:message var="income" code="income"></spring:message>
<spring:message var="release_num" code="release_num"></spring:message>
<spring:message var="cmpl" code="cmpl"></spring:message>
<spring:message var="reg_time" code="reg_time"></spring:message>
<spring:message var="ship_date" code="ship_date"></spring:message>
<spring:message var="ship_cnt" code="ship_cnt"></spring:message>
<spring:message var="cnt_more_than_complete_stock" code="cnt_more_than_complete_stock"></spring:message>
<spring:message var="save_ok" code="save_ok"></spring:message>
<spring:message var="ok" code="ok"></spring:message>
<spring:message var="divide" code="divide"></spring:message>
<spring:message var="ship_history" code="ship_history"></spring:message>
<spring:message var="move_operation" code="move_operation"></spring:message>
<spring:message var="current_operation" code="current_operation"></spring:message>
<spring:message var="stock" code="stock"></spring:message>
<spring:message var="operation" code="operation"></spring:message>
<spring:message var="move_cnt" code="move_cnt"></spring:message>
<spring:message var="cnt_more_than_stock" code="cnt_more_than_stock"></spring:message>
<spring:message var="release_date" code="release_date"></spring:message>
<spring:message var="divide_lot" code="divide_lot"></spring:message>
<spring:message var="mat_prd_no" code="mat_prd_no"></spring:message>
<spring:message var="current_operation" code="current_operation"></spring:message>
<spring:message var="stock_status" code="stock_status"></spring:message>
<spring:message var="division" code="division"></spring:message>
<spring:message var="stock_cnt" code="stock_cnt"></spring:message>
<spring:message var="lead_time" code="lead_time"></spring:message>
<spring:message var="cancel" code="cancel"></spring:message>
<spring:message var="chk_del" code="chk_del"></spring:message>
<spring:message var="operating_time_sec" code="operating_time_sec"></spring:message>
<spring:message var="operation_time_ratio" code="operation_time_ratio"></spring:message>
<spring:message var="cutting_time_sec" code="cutting_time_sec"></spring:message>
<spring:message var="cutting_time_ratio_sec" code="cutting_time_ratio_sec"></spring:message>
<spring:message var="non_operation_time_sec" code="non_operation_time_sec"></spring:message>
<spring:message var="non_operation_time_ratio" code="non_operation_time_ratio"></spring:message>
<spring:message var="operating_wait_sec" code="operating_wait_sec"></spring:message>
<spring:message var="operating_wait_ratio" code="operating_wait_ratio"></spring:message>
<spring:message var="total_sec" code="total_sec"></spring:message>
<spring:message var="faulty_ratio_customr_and_operation" code="faulty_ratio_customr_and_operation"></spring:message>
<spring:message var="prd_cnt" code="prd_cnt"></spring:message>
<spring:message var="faulty_cnt_operation" code="faulty_cnt_operation"></spring:message>
<spring:message var="faulty_cnt_customer" code="faulty_cnt_customer"></spring:message>
<spring:message var="operation_faulty_operation" code="operation_faulty_operation"></spring:message>
<spring:message var="operation_faulty_customer" code="operation_faulty_customer"></spring:message>
<spring:message var="f_m_l_check" code="f_m_l_check"></spring:message>
<spring:message var="total" code="total"></spring:message>
<spring:message var="machine_status" code="machine_status"></spring:message>
<spring:message var="dmm" code="dmm"></spring:message>
<spring:message var="comname" code="comname"></spring:message>
<spring:message var="dmm" code="dmm"></spring:message>
<spring:message var="dashboard_emo" code="dashboard_emo"></spring:message>
<spring:message var="dashboard_doosan" code="dashboard_doosan"></spring:message>
<spring:message var="dashboard_bk" code="dashboard_bk"></spring:message>
<spring:message var="dashboard_bk2" code="dashboard_bk2"></spring:message>
<spring:message var="total_machine_prd_status" code="total_machine_prd_status"></spring:message>
<spring:message var="24chart_title" code="24chart_title"></spring:message>

<%--  공통 context 부분  --%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/> 
<c:set var="newline" value="<%= \"\n\" %>" />
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>

<script>
	var shopId = 1;
	var ctxPath = "${ctxPath}";
	var comName = "${comname}";
	
	var servlet = "/DULink"
	var monitor_nav = [
	                  	["${layout}", servlet + "/chart/main.do"],
	                  	/* ["${dmm}","/DMM/index.do"], */
	                  	/* ["${total_machine_prd_status}", "/Machine_Prdc_Status/index.do"], */
	                		["${dailydevicestatus}","/Single_Chart_Status/index.do"],
	                   	/* ["${prdct_board}", "prdStatus.do"], */
	                   	["${barchart}", "/24hChart/index.do"],
	                   	["${prdct_board}", "/Prdc_Board/index.do"],
	                  	/* ["${machine_status}", servlet + "/chart/machine_status.do"] */
	                  ];
	
	var inven_nav = [
                    	["${income_manage}", "incomeStock.do"],
                    	["${income_history}","incomeStockHistory.do"],
                    	["${release_manage}", "releaseStock.do"],
                    	["${release_history}", "releaseStockHistory.do"],
                    	["${ship_manage}", "shipment.do"],
                    	["${ship_history}", "shipmentHistory.do"],
                    	["${move_operation}","transferOpr.do"],
                    	["${stock_status}","stockStatus.do"]
                   ];
	
	var analysis_nav = [
		               	["${performance_chart}", "performanceAgainstGoal_chart.do"],
		                ["${operation_chart}","jigGraph.do"],
		                ["${operation_graph_daily}", "wcGraph.do"],
		                ["${check_program_error}", "programManager.do"]
		               ];
	
	var kpi_nav = [
                   ["${performance_chart}", "performanceAgainstGoal_chart_kpi.do"],
                   ["${operation_chart}","jigGraph_kpi.do"],
                   ["${lead_time}", "leadTime.do"],
                   ["${faulty_ratio_customr_and_operation}", "faulty_kpi.do"]
                  ];
	
	var quality_nav = [
                   ["${f_m_l_check}", "checkPrdct.do"],
                   ["${check_result_manage}","checkPrdctStandard.do"],
                   ["${add_faulty}", "addFaulty.do"],
                   ["${add_faulty_hist}", "addFaultyHistory.do"],
                   ["${faulty_ratio_customr_and_operation}", "faulty.do"],
                   ["${lot_tracer}", "lotTracer.do"],
                  ];
	
	var tool_nav = [
	                   ["${tool_manage}", "toolManager.do"],
	                   ["${check_program_error}","programManager_tool.do"],
	                   ["${reoperation_cycle}", "reOpCycle.do"]
	                  ];
	
	var order_nav = [
	                   ["${add_prdct_target}", "addTarget.do"],
	                   ["${add_cmpl_prdct}","addPrdCmpl.do"],
	                   ["${add_cmpl_prdct_history}", "addPrdCmplHistory.do"],
	                   ["${add_non_operation}", "addNoOperation.do"],
	                   ["${add_non_operation_hist}", "addNoOperationHistory.do"]
	                  ];
	
	var mainten_nav = [
	                   ["${alarm_manage}", "alarmReport.do"],
	                   ["${machine_list}","traceManager.do"],
	                   ["Catch Phrase", "banner.do"],
	                   //["TV Control", ""],
	                   ["${worker_manage}", "workerMaanger.do"]
	                  ];
	
	var config_nav = [
	                   ["${alarm_manage}", "/RONAL/chart/alarmReport.do"],
	                   ["${machine_list}","/RONAL/chart/traceManager.do"],
	                   ["Catch Phrase", "/RONAL/chart/banner.do"],
	                   ["${layoutManager}", "/Layout_Manager/index.do"],
	                   ["${deviceManager}", "/Device_Manager/index.do"],
	                   //["TV Control", ""],
	                  ];
	
	
	
	function createNav(ty, idx){
		var nav = eval(ty);
		var color;
		if(ty=="monitor_nav"){
			color = "blue"
		}else if(ty=="analysis_nav" || ty == "kpi_nav"){
			color = "green"
		}else{
			color = "purple"
		}
		
		for(var i = 0; i < nav.length; i++){
			$(".nav_span:nth(" + i + ")").html(nav[i][0]).addClass("nav").attr("url",nav[i][1]);
			$(".nav_span:nth(" + i + ")").next().addClass("nav").attr("url",nav[i][1]);
		}
		$(".nav_span:nth(" + idx + ")").attr("id", "selected");
		$(".nav").next().attr("src", ctxPath + "/images/unselected.png");
		$(".nav_span:nth(" + idx + ")").next().attr("src", ctxPath + "/images/selected_" + color + ".png");
		$("#table img:nth(3)").attr("src", ctxPath + "/images/" + color + "_right.png");
		$("#table img:nth(2)").attr("src", ctxPath + "/images/" + ty.substr(0, ty.lastIndexOf("_")) + "_left.png");
		
		$(".nav").click(_navigator);
		
	};

	function getToday(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var hour = date.getHours();
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		var ty;
		
		if(hour>12){
			ty = "${pm}";
			hour -= 12;
		}else{
			ty = "${am}";
		};
		
		
		return year + "-" + month + "-" + day + " " + ty + " " + hour + " : " + minute + " : " + second;
	};
	
	function chkLang(){
		var str = $("#time").html();
		var check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
		if(check.test(str)){
			return "ko";
		}else{
			return "other";
		}
	};
	
	function _navigator(){
		var url = $(this).attr('url');
		console.log(url)
		location.href =  url + "?lang=" + window.localStorage.getItem("lang");
	};

	/* var login = window.sessionStorage.getItem("login");
	var login_time = window.sessionStorage.getItem("login_time");
	var time = new Date().getTime();
	if(login==null || (login !=null && (time - login_time) / 1000 > 60*20)) {
		if(window.location.pathname.indexOf("/chart/index.do")==-1){
			alert("세션이 만료 되었습니다.");
			location.href = "${ctxPath}/chart/index.do";			
		}
	} */
</script>
<script type="text/javascript" src="${ctxPath }/js/default.js"></script>
<!-- <script type="text/javascript" src="/Device_Status/js/default.js "></script> -->
<%-- <link rel="stylesheet" type="text/css" href="${ctxPath}/css/tableForm.css"> --%>
<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"> -->

